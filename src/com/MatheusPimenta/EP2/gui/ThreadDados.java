package com.MatheusPimenta.EP2.gui;

import java.io.IOException;
import java.util.List;

import javax.xml.bind.JAXBException;

import edu.unb.fga.dadosabertos.Camara;
import edu.unb.fga.dadosabertos.Deputado;

public class ThreadDados extends Thread {
	
	public static Deputado deputado;
	public static List<Deputado> deputados;
	
	public void run(){
		Camara camara = new Camara();
		try {
			camara.obterDados();
		} catch (JAXBException | IOException e) {
			e.printStackTrace();
		}
		deputados = camara.getDeputados();
		
		for(int i = 0;i<deputados.size();i++){
			deputado = deputados.get(i);
			try {
				deputado.obterDetalhes();
			} catch (IOException | JAXBException e) {
				e.printStackTrace();
			}
		}
	}
}
