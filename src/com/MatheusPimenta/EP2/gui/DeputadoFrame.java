package com.MatheusPimenta.EP2.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import edu.unb.fga.dadosabertos.Camara;
import edu.unb.fga.dadosabertos.Deputado;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.xml.bind.JAXBException;

public class DeputadoFrame extends JFrame {
	
	JLabel rotuloCaixa,espaco,titulo,titulo2,subtitulo,subtitulo2,espaco4,espaco2,espaco3,qntdDeElementos,qntdDePessoas;
	JPanel painelAbaDeputado, painelAbaPartido,painelBuscas,painelBuscas2,painelListas,painelListas2,painelTitulo,painelTitulo2;
	JButton pesquisar,ordenarLista,mostrarDetalhes,pesquisarPartido;
	JTextField caixaPesquisaDeputados,caixa,caixa2;
	JComboBox listaTipoProcura,listaTipoPartido;
	DefaultListModel listaModelo,listaModeloPartido;
	JList lista,lista2;
	List<Deputado> listaNova,listaNovaPartido;
	
	public DeputadoFrame() throws JAXBException, IOException{

		super("Deputados do Brasil");
		
		criarAbas();
		criarMenu();
	}
	
	public void criarAbas() throws JAXBException, IOException {
		Font fonte = new Font("Palatino Linotype",Font.PLAIN,26);
		Font fonte2 = new Font("Palatino Linotype",Font.PLAIN,15);
		
		Pesquisar pesquisarNome = new Pesquisar();
		PesquisarPartido pesquisarPartidoMetodo = new PesquisarPartido();
		DetalharDeputado detalharDeputado = new DetalharDeputado();
		
		painelAbaDeputado = new JPanel();
		painelAbaDeputado.setLayout(new BorderLayout());
		painelAbaPartido = new JPanel();
		painelAbaPartido.setLayout(new BorderLayout());
		JTabbedPane container = new JTabbedPane();
		container.add("Deputados",painelAbaDeputado);
		container.add("Partidos", painelAbaPartido);
		
		//AbaDeputado
		painelTitulo = new JPanel();
		titulo = new JLabel("Busca de Deputados");
		titulo.setFont(fonte);
		painelTitulo.add(titulo);
		
		painelBuscas = new JPanel();
		painelBuscas.setLayout(new FlowLayout());
		
		String[] tipoProcura = {"Ordenar por ","Nome", "Suplente -> Titular","Titular -> Suplente"};
		listaTipoProcura = new JComboBox(tipoProcura);
		
		Ordenar ordenar = new Ordenar();

		rotuloCaixa = new JLabel("Pesquisar");
		rotuloCaixa.setFont(fonte2);
		ordenarLista = new JButton("Ordenar");
		ordenarLista.addActionListener(ordenar);
		espaco = new JLabel("    ");
		espaco2 = new JLabel("            ");
		pesquisar = new JButton("Ir");
		pesquisar.addActionListener(pesquisarNome);
		caixaPesquisaDeputados = new JTextField(10);
		subtitulo = new JLabel("Relação de Políticos");
		subtitulo.setFont(new Font("Palatino Linotype",Font.PLAIN,24));
		
		painelBuscas.add(listaTipoProcura);
		painelBuscas.add(ordenarLista);
		painelBuscas.add(espaco);
		painelBuscas.add(rotuloCaixa);
		painelBuscas.add(caixaPesquisaDeputados);
		painelBuscas.add(pesquisar);
		painelBuscas.add(espaco2);
		painelBuscas.add(subtitulo);
		
		painelListas = new JPanel();
		painelListas.setLayout(new FlowLayout()); 
		
		listaNova = new ArrayList();
		listaModelo = new DefaultListModel();
		
		for(int i = 0;i<ProgramaPrincipal.deputados.size();i++){
			Deputado deputado2 = ProgramaPrincipal.deputados.get(i);
			listaModelo.addElement(deputado2.getNome());
			listaNova.add(ProgramaPrincipal.deputados.get(i));
		}
		lista = new JList(listaModelo);
		JScrollPane painel = new JScrollPane(lista);
		
		qntdDeElementos = new JLabel("Quantidade: ");
		qntdDeElementos.setFont(new Font("Palatino Linotype",Font.PLAIN,12));
		
		caixa = new JTextField(3);
		caixa.setText(" "+listaModelo.getSize());
		
		mostrarDetalhes = new JButton("Ver Detalhes");
		mostrarDetalhes.addActionListener(detalharDeputado);
		
		painelListas.add(painel);
		painelListas.add(mostrarDetalhes);
		painelListas.add(qntdDeElementos);
		painelListas.add(caixa);

		painelAbaDeputado.add(painelTitulo,BorderLayout.NORTH);
		painelAbaDeputado.add(painelBuscas,BorderLayout.CENTER);
		painelAbaDeputado.add(painelListas,BorderLayout.SOUTH);
		
		//AbaPartido
		painelTitulo2 = new JPanel();
		titulo2 = new JLabel("Busca de Deputados por Partido");
		titulo2.setFont(fonte);
		painelTitulo2.add(titulo2);
		
		painelBuscas2 = new JPanel();
		painelBuscas2.setLayout(new FlowLayout());
		
		String[] partidos = {"Mostrar ","PDT","PROS","PMDB","PCdoB","PSD","PT","PSDB","PR","PTdoB","REDE","PSOL","PSDC","DEM","PSB","PTB","PEN","PP","PRB","SD","PPS","PHS","PSC","PMN","S.PART.","PTN","PSL","PV"};
		listaTipoPartido = new JComboBox(partidos);
		pesquisarPartido = new JButton("Ir");
		pesquisarPartido.addActionListener(pesquisarPartidoMetodo);
		subtitulo2 = new JLabel("Políticos Relacionados");
		subtitulo2.setFont(fonte);
		espaco3 = new JLabel("                                                  ");
		espaco4 = new JLabel("                                      ");
		
		painelBuscas2.add(espaco4);
		painelBuscas2.add(listaTipoPartido);
		painelBuscas2.add(pesquisarPartido);
		painelBuscas2.add(espaco3);
		painelBuscas2.add(subtitulo2);
		
		painelListas2 = new JPanel();
		painelListas2.setLayout(new FlowLayout());
		
		listaModeloPartido = new DefaultListModel();
		for(int i = 0;i<10;i++){
			listaModeloPartido.addElement("                        ");
		}
		lista2 = new JList(listaModeloPartido);
		JScrollPane painel2 = new JScrollPane(lista2);
		
		qntdDePessoas = new JLabel("Pessoas nesse Partido: ");
		qntdDePessoas.setFont(fonte2);
		caixa2 = new JTextField(3);
		caixa2.setText("0");
		
		painelListas2.add(painel2);
		painelListas2.add(qntdDePessoas);
		painelListas2.add(caixa2);
		
		painelAbaPartido.add(painelTitulo2,BorderLayout.NORTH);
		painelAbaPartido.add(painelBuscas2,BorderLayout.CENTER);
		painelAbaPartido.add(painelListas2,BorderLayout.SOUTH);
		
		add(container);
		
	}
	
	public void criarMenu(){
		JMenu menuDeputado = new JMenu("Deputado");
		
		SobreAction sobreAction = new SobreAction();
		FecharAction fecharAction = new FecharAction();
		NovoAction novoAction = new NovoAction();
		
		JMenuItem novaPesquisa = new JMenuItem("Nova Pesquisa");
		novaPesquisa.addActionListener(novoAction);
		menuDeputado.add(novaPesquisa);
		
		JMenuItem fechar = new JMenuItem("Sair");
		fechar.addActionListener(fecharAction);
		menuDeputado.add(fechar);
		
		JMenu menuAjuda = new JMenu("Ajuda");
		
		JMenuItem menuItemSobre = new JMenuItem("Sobre...");
		menuItemSobre.addActionListener(sobreAction);
		menuAjuda.add(menuItemSobre);
		
		JMenuBar barra = new JMenuBar();
		setJMenuBar(barra);
		barra.add(menuDeputado);
		barra.add(menuAjuda);
		
		
	}
	
	private class SobreAction implements ActionListener{

		public void actionPerformed(ActionEvent arg0) {
			JOptionPane.showMessageDialog(null, "Segundo trabalho prático de OO feito por: Matheus Figueiredo.", "Informações", JOptionPane.PLAIN_MESSAGE);
			
		}
		
	}
	
	private class DetalharDeputado implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			int indice = lista.getSelectedIndex();
			
			String[] nome = new String[listaNova.size()];
			String[] nascimento = new String[listaNova.size()];
			String[] partido = new String[listaNova.size()];
			int[] id = new int[listaNova.size()];
			String[] sexo = new String[listaNova.size()];
			String[] email = new String[listaNova.size()];
			int[] matricula = new int[listaNova.size()];
			String[] condicao = new String[listaNova.size()];
			String[] numeroGabinete = new String[listaNova.size()];
			String[] telefoneGabinete = new String[listaNova.size()];
			String[] anexo = new String[listaNova.size()];

			
			for(int i = 0;i<listaNova.size();i++){
				nome[i] = listaNova.get(i).getNome();
				nascimento[i] = listaNova.get(i).getDetalhes().getDataNascimento();
				partido[i] = listaNova.get(i).getPartido();
				id[i] = listaNova.get(i).getIdeCadastro();
				sexo[i] = listaNova.get(i).getSexo();
				email[i] = listaNova.get(i).getEmail();
				matricula[i] = listaNova.get(i).getMatricula();
				condicao[i] = listaNova.get(i).getCondicao();
				numeroGabinete[i] = listaNova.get(i).getGabinete();
				telefoneGabinete[i] = listaNova.get(i).getFone();
				anexo[i] = listaNova.get(i).getAnexo();
				
			}
			
			JOptionPane.showMessageDialog(null, "Nome: "+nome[indice]+"\nPartido: "+partido[indice]+"\nData de Nascimento: "+nascimento[indice]+"\nId: "+id[indice]+"\nSexo: "+sexo[indice]+"\nE-mail: "+email[indice]+"\nMatricula: "+matricula[indice]+"\nCondição: "+condicao[indice]+"\nAnexo: "+anexo[indice]+"\nNumero do Gabinete: "+numeroGabinete[indice]+"\nTelefone do Gabinete: "+telefoneGabinete[indice],
					""+nome[indice].toUpperCase(),JOptionPane.PLAIN_MESSAGE );
		}
		
	}
	
	private class PesquisarPartido implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			
			listaModeloPartido.clear();
			
			if(listaTipoPartido.getSelectedIndex() == 1){
				for(int i = 0;i<ProgramaPrincipal.deputados.size();i++){
					Deputado deputado2 = ProgramaPrincipal.deputados.get(i);
					if(deputado2.getPartido().toUpperCase().equals("PDT")){
						listaModeloPartido.addElement(deputado2.getNome());
					}
				}
			}
			else
				if(listaTipoPartido.getSelectedIndex() == 2){
					for(int i = 0;i<ProgramaPrincipal.deputados.size();i++){
						Deputado deputado2 = ProgramaPrincipal.deputados.get(i);
						if(deputado2.getPartido().toUpperCase().equals("PROS")){
							listaModeloPartido.addElement(deputado2.getNome());
						}
					}
				}
				else
					if(listaTipoPartido.getSelectedIndex() == 3){
						for(int i = 0;i<ProgramaPrincipal.deputados.size();i++){
							Deputado deputado2 = ProgramaPrincipal.deputados.get(i);
							if(deputado2.getPartido().toUpperCase().equals("PMDB")){
								listaModeloPartido.addElement(deputado2.getNome());
							}
						}
					}
					else
						if(listaTipoPartido.getSelectedIndex() == 4){
							for(int i = 0;i<ProgramaPrincipal.deputados.size();i++){
								Deputado deputado2 = ProgramaPrincipal.deputados.get(i);
								if(deputado2.getPartido().toUpperCase().equals("PCdoB")){
									listaModeloPartido.addElement(deputado2.getNome());
								}
							}
						}
						else
							if(listaTipoPartido.getSelectedIndex() == 5){
								for(int i = 0;i<ProgramaPrincipal.deputados.size();i++){
									Deputado deputado2 = ProgramaPrincipal.deputados.get(i);
									if(deputado2.getPartido().toUpperCase().equals("PSD")){
										listaModeloPartido.addElement(deputado2.getNome());
									}
								}
							}
							else
								if(listaTipoPartido.getSelectedIndex() == 6){
									for(int i = 0;i<ProgramaPrincipal.deputados.size();i++){
										Deputado deputado2 = ProgramaPrincipal.deputados.get(i);
										if(deputado2.getPartido().toUpperCase().equals("PT")){
											listaModeloPartido.addElement(deputado2.getNome());
										}
									}
								}
								else
									if(listaTipoPartido.getSelectedIndex() == 7){
										for(int i = 0;i<ProgramaPrincipal.deputados.size();i++){
											Deputado deputado2 = ProgramaPrincipal.deputados.get(i);
											if(deputado2.getPartido().toUpperCase().equals("PSDB")){
												listaModeloPartido.addElement(deputado2.getNome());
											}
										}
									}
									else
										if(listaTipoPartido.getSelectedIndex() == 8){
											for(int i = 0;i<ProgramaPrincipal.deputados.size();i++){
												Deputado deputado2 = ProgramaPrincipal.deputados.get(i);
												if(deputado2.getPartido().toUpperCase().equals("PR")){
													listaModeloPartido.addElement(deputado2.getNome());
												}
											}
										}
										else
											if(listaTipoPartido.getSelectedIndex() == 9){
												for(int i = 0;i<ProgramaPrincipal.deputados.size();i++){
													Deputado deputado2 = ProgramaPrincipal.deputados.get(i);
													if(deputado2.getPartido().toUpperCase().equals("PTdoB")){
														listaModeloPartido.addElement(deputado2.getNome());
													}
												}
											}
											else
												if(listaTipoPartido.getSelectedIndex() == 10){
													for(int i = 0;i<ProgramaPrincipal.deputados.size();i++){
														Deputado deputado2 = ProgramaPrincipal.deputados.get(i);
														if(deputado2.getPartido().toUpperCase().equals("REDE")){
															listaModeloPartido.addElement(deputado2.getNome());
														}
													}
												}
												else
													if(listaTipoPartido.getSelectedIndex() == 11){
														for(int i = 0;i<ProgramaPrincipal.deputados.size();i++){
															Deputado deputado2 = ProgramaPrincipal.deputados.get(i);
															if(deputado2.getPartido().toUpperCase().equals("PSOL")){
																listaModeloPartido.addElement(deputado2.getNome());
															}
														}
													}
													else
														if(listaTipoPartido.getSelectedIndex() == 12){
															for(int i = 0;i<ProgramaPrincipal.deputados.size();i++){
																Deputado deputado2 = ProgramaPrincipal.deputados.get(i);
																if(deputado2.getPartido().toUpperCase().equals("PSDC")){
																	listaModeloPartido.addElement(deputado2.getNome());
																}
															}
														}
														else
															if(listaTipoPartido.getSelectedIndex() == 13){
																for(int i = 0;i<ProgramaPrincipal.deputados.size();i++){
																	Deputado deputado2 = ProgramaPrincipal.deputados.get(i);
																	if(deputado2.getPartido().toUpperCase().equals("DEM")){
																		listaModeloPartido.addElement(deputado2.getNome());
																	}
																}
															}
															else
																if(listaTipoPartido.getSelectedIndex() == 14){
																	for(int i = 0;i<ProgramaPrincipal.deputados.size();i++){
																		Deputado deputado2 = ProgramaPrincipal.deputados.get(i);
																		if(deputado2.getPartido().toUpperCase().equals("PSB")){
																			listaModeloPartido.addElement(deputado2.getNome());
																		}
																	}
																}
																else
																	if(listaTipoPartido.getSelectedIndex() == 15){
																		for(int i = 0;i<ProgramaPrincipal.deputados.size();i++){
																			Deputado deputado2 = ProgramaPrincipal.deputados.get(i);
																			if(deputado2.getPartido().toUpperCase().equals("PTB")){
																				listaModeloPartido.addElement(deputado2.getNome());
																			}
																		}
																	}
																	else
																		if(listaTipoPartido.getSelectedIndex() == 16){
																			for(int i = 0;i<ProgramaPrincipal.deputados.size();i++){
																				Deputado deputado2 = ProgramaPrincipal.deputados.get(i);
																				if(deputado2.getPartido().toUpperCase().equals("PEN")){
																					listaModeloPartido.addElement(deputado2.getNome());
																				}
																			}
																		}
																		else
																			if(listaTipoPartido.getSelectedIndex() == 17){
																				for(int i = 0;i<ProgramaPrincipal.deputados.size();i++){
																					Deputado deputado2 = ProgramaPrincipal.deputados.get(i);
																					if(deputado2.getPartido().toUpperCase().equals("PP")){
																						listaModeloPartido.addElement(deputado2.getNome());
																					}
																				}
																			}
																			else
																				if(listaTipoPartido.getSelectedIndex() == 18){
																					for(int i = 0;i<ProgramaPrincipal.deputados.size();i++){
																						Deputado deputado2 = ProgramaPrincipal.deputados.get(i);
																						if(deputado2.getPartido().toUpperCase().equals("PRB")){
																							listaModeloPartido.addElement(deputado2.getNome());
																						}
																					}
																				}
																				else
																					if(listaTipoPartido.getSelectedIndex() == 19){
																						for(int i = 0;i<ProgramaPrincipal.deputados.size();i++){
																							Deputado deputado2 = ProgramaPrincipal.deputados.get(i);
																							if(deputado2.getPartido().toUpperCase().equals("SD")){
																								listaModeloPartido.addElement(deputado2.getNome());
																							}
																						}
																					}
																					else
																						if(listaTipoPartido.getSelectedIndex() == 20){
																							for(int i = 0;i<ProgramaPrincipal.deputados.size();i++){
																								Deputado deputado2 = ProgramaPrincipal.deputados.get(i);
																								if(deputado2.getPartido().toUpperCase().equals("PPS")){
																									listaModeloPartido.addElement(deputado2.getNome());
																								}
																							}
																						}
																						else
																							if(listaTipoPartido.getSelectedIndex() == 21){
																								for(int i = 0;i<ProgramaPrincipal.deputados.size();i++){
																									Deputado deputado2 = ProgramaPrincipal.deputados.get(i);
																									if(deputado2.getPartido().toUpperCase().equals("PHS")){
																										listaModeloPartido.addElement(deputado2.getNome());
																									}
																								}
																							}
																							else
																								if(listaTipoPartido.getSelectedIndex() == 22){
																									for(int i = 0;i<ProgramaPrincipal.deputados.size();i++){
																										Deputado deputado2 = ProgramaPrincipal.deputados.get(i);
																										if(deputado2.getPartido().toUpperCase().equals("PSC")){
																											listaModeloPartido.addElement(deputado2.getNome());
																										}
																									}
																								}
																								else
																									if(listaTipoPartido.getSelectedIndex() == 23){
																										for(int i = 0;i<ProgramaPrincipal.deputados.size();i++){
																											Deputado deputado2 = ProgramaPrincipal.deputados.get(i);
																											if(deputado2.getPartido().toUpperCase().equals("PMN")){
																												listaModeloPartido.addElement(deputado2.getNome());
																											}
																										}
																									}
																									else
																										if(listaTipoPartido.getSelectedIndex() == 24){
																											for(int i = 0;i<ProgramaPrincipal.deputados.size();i++){
																												Deputado deputado2 = ProgramaPrincipal.deputados.get(i);
																												if(deputado2.getPartido().toUpperCase().equals("S.PART.")){
																													listaModeloPartido.addElement(deputado2.getNome());
																												}
																											}
																										}
																										else
																											if(listaTipoPartido.getSelectedIndex() == 25){
																												for(int i = 0;i<ProgramaPrincipal.deputados.size();i++){
																													Deputado deputado2 = ProgramaPrincipal.deputados.get(i);
																													if(deputado2.getPartido().toUpperCase().equals("PTN")){
																														listaModeloPartido.addElement(deputado2.getNome());
																													}
																												}
																											}
																											else
																												if(listaTipoPartido.getSelectedIndex() == 26){
																													for(int i = 0;i<ProgramaPrincipal.deputados.size();i++){
																														Deputado deputado2 = ProgramaPrincipal.deputados.get(i);
																														if(deputado2.getPartido().toUpperCase().equals("PSL")){
																															listaModeloPartido.addElement(deputado2.getNome());
																														}
																													}
																												}
																												else
																													if(listaTipoPartido.getSelectedIndex() == 27){
																														for(int i = 0;i<ProgramaPrincipal.deputados.size();i++){
																															Deputado deputado2 = ProgramaPrincipal.deputados.get(i);
																															if(deputado2.getPartido().toUpperCase().equals("PV")){
																																listaModeloPartido.addElement(deputado2.getNome());
																															}
																														}
																													}
			caixa2.setText(""+listaModeloPartido.size());
		}
		
	}
	
	private class Ordenar implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent er) {
			if(listaTipoProcura.getSelectedIndex() == 1){
				String[] nome = new String[listaNova.size()];
				
				for(int i = 0;i<listaNova.size();i++){
					nome[i] = listaNova.get(i).getNome();
				}
				
				Arrays.sort(nome);
				
				listaModelo.clear();
				
				for(int i = 0;i<nome.length;i++){
					listaModelo.addElement(nome[i]);
				}

			}
			else{
				if(listaTipoProcura.getSelectedIndex() == 2){					
					String[] suplentes = new String[listaNova.size()];
					String[] titular = new String[listaNova.size()];
					
					for(int i = 0;i<listaNova.size();i++){
						if(listaNova.get(i).getCondicao().equals("Suplente"))
							suplentes[i] = listaNova.get(i).getNome();
					}
					
					for(int i = 0;i<listaNova.size();i++){
						if(listaNova.get(i).getCondicao().equals("Titular"))
							titular[i] = listaNova.get(i).getNome();
					}
					
					listaModelo.clear();
					
					for(int i = 0;i<suplentes.length;i++){
						if(suplentes[i]!=null)
							listaModelo.addElement(suplentes[i]);
					}
					for(int i = 0;i<titular.length;i++){
						if(titular[i]!=null)
							listaModelo.addElement(titular[i]);
					}

					caixa.setText(" "+listaModelo.getSize());		
				}
				else{
					if(listaTipoProcura.getSelectedIndex() == 3){
						String[] titular2 = new String[listaNova.size()];
						String[] suplentes2 = new String[listaNova.size()];
						
						for(int i = 0;i<listaNova.size();i++){
							if(listaNova.get(i).getCondicao().equals("Titular"))
								titular2[i] = listaNova.get(i).getNome();
						}
						
						for(int i = 0;i<listaNova.size();i++){
							if(listaNova.get(i).getCondicao().equals("Suplente"))
								suplentes2[i] = listaNova.get(i).getNome();
						}
						
						listaModelo.clear();
						
						for(int i = 0;i<titular2.length;i++){
							if(titular2[i]!=null)
								listaModelo.addElement(titular2[i]);
						}
						for(int i = 0;i<suplentes2.length;i++){
							if(suplentes2[i]!=null)
								listaModelo.addElement(suplentes2[i]);
						}
						caixa.setText(" "+listaModelo.getSize());	
					}
				}
			}
		}
	}
	
	private class Pesquisar implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			listaNova = new ArrayList();
			
			if(caixaPesquisaDeputados.getText().equals("")){
				JOptionPane.showMessageDialog(null, "Digite o nome a ser pesquisado!");
				caixaPesquisaDeputados.requestFocus();
			}
			else{
				listaModelo.clear();
				
				for(int i = 0;i<ProgramaPrincipal.deputados.size();i++){
					Deputado deputado2 = ProgramaPrincipal.deputados.get(i);
					if(deputado2.getNome().contains(caixaPesquisaDeputados.getText().toUpperCase())){
						listaNova.add(ProgramaPrincipal.deputados.get(i));
						listaModelo.addElement(deputado2.getNome());
					}
				}
				if(listaModelo.isEmpty()){
					JOptionPane.showMessageDialog(null, "Sua busca não obteve resultado, tente novamente!");
					caixaPesquisaDeputados.requestFocus();
				}
				caixa.setText(" "+listaModelo.getSize());			
			}
		}
		
	}
	
	private class FecharAction implements ActionListener{

		public void actionPerformed(ActionEvent event) {
			System.exit(0);			
		}
		
	}
	
	private class NovoAction implements ActionListener{
		
	public void actionPerformed(ActionEvent event) {
			caixaPesquisaDeputados.setText("");

			listaModelo.clear();
			
			for(int i = 0;i<ProgramaPrincipal.deputados.size();i++){
				Deputado deputado2 = ProgramaPrincipal.deputados.get(i);

				listaModelo.addElement(deputado2.getNome());
				listaNova.add(ProgramaPrincipal.deputados.get(i));
			}
			caixa.setText(" "+listaModelo.getSize());	
		}
		
	}
}




