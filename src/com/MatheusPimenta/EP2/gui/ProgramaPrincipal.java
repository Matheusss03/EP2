package com.MatheusPimenta.EP2.gui;

import java.io.IOException;
import java.util.List;

import javax.swing.JFrame;
import javax.xml.bind.JAXBException;

import edu.unb.fga.dadosabertos.Camara;
import edu.unb.fga.dadosabertos.Deputado;

public class ProgramaPrincipal {
	
	public static Deputado deputado;
	public static List<Deputado> deputados;

	public static void main(String[] args){
		
		buscarDados();
		executarInterface();
		
	}
	
	public static void buscarDados(){
		Camara camara = new Camara();
		try {
			camara.obterDados();
		} catch (JAXBException | IOException e) {
			e.printStackTrace();
		}
		deputados = camara.getDeputados();
		
		for(int i = 0;i<deputados.size();i++){
			deputado = deputados.get(i);
			
			try {
				deputado.obterDetalhes();
			} catch (IOException | JAXBException e) {
				e.printStackTrace();
			}
			
		}
		
	}
	
	public static void executarInterface(){

		DeputadoFrame deputadoFrame = null;
		try {
			deputadoFrame = new DeputadoFrame();
		} catch (JAXBException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		deputadoFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		deputadoFrame.setSize(740, 332);
		deputadoFrame.isResizable();
		deputadoFrame.setVisible(true);
	}
	
}
